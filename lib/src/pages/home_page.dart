import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_flutter/src/pages/sign_in_page.dart';
import 'package:flutter/material.dart';

import '../firebase/firebase_auth_services.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final TextEditingController _infoController = TextEditingController();
  final FirebaseAuthService firebase = FirebaseAuthService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home Page'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: ListView(
          children: [
            TextFormField(
              controller: _infoController,
              decoration: const InputDecoration(
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(30),
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(30),
                  ),
                ),
                hintText: 'Digita una Informacion',
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
              onPressed: () {
                setState(() {
                  firebase.db
                      .collection("data")
                      .add({'info': _infoController.text});
                  _infoController.clear();
                });
              },
              child: const Text('Guardar Informacion'),
            ),
            const SizedBox(
              height: 20,
            ),
            FutureBuilder(
              future: firebase.getData(),
              builder: (BuildContext context, snapshot) {
                if (snapshot.hasData) {
                  return SizedBox(
                    height: 400,
                    child: ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (context, index) {
                        return ListTile(
                          leading: Text(index.toString()),
                          title: Text('${snapshot.data[index]["info"]}'),
                        );
                      },
                    ),
                  );
                }
                return const SizedBox();
              },
            ),
            const SizedBox(
              height: 400,
            ),
            ElevatedButton(
              onPressed: () {
                FirebaseAuth.instance.signOut();
                Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(
                    builder: (context) => const SignInPage(),
                  ),
                  (Route<dynamic> route) => false,
                );
              },
              child: const Text('Sign Out'),
            ),
          ],
        ),
      ),
    );
  }
}
